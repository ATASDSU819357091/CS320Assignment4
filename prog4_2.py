# Stack Machine that will take already valid tokens and execute their functions
import sys
class StackMachine:
    def __init__(self):
        self.stack = []
        self.CurrentLine = 0
        self.saveMemo = {}
    def Execute(self,tokens):
        # Pops the top of the stack. Returns the popped value.
        if tokens[0] == "push":
            self.stack.append(int(tokens[1]))
            self.CurrentLine = self.CurrentLine + 1
        # Pops the top of the stack. Returns the popped value.
        if tokens[0] == "pop":
            if len(self.stack) != 0:
                self.CurrentLine = self.CurrentLine + 1
                return self.stack.pop()
            else:
                raise IndexError("Invalid Memory Access")
        # Pops two values off the stack, adds them pushes the result.
        if tokens[0] == "add":
            if len(self.stack) >= 2:
                tmp1value = self.stack.pop()
                tmp2value = self.stack.pop()
                self.stack.append(int(tmp1value)+ int(tmp2value))
                self.CurrentLine = self.CurrentLine + 1
            else:
                raise IndexError("Invalid Memory Access")
        # Pops two, subtracts the second from the first, pushes result.
        if tokens[0] == "sub":
            if len(self.stack) >= 2:
                tmp1value = self.stack.pop()
                tmp2value = self.stack.pop()
                self.stack.append(int(tmp1value)-int(tmp2value))
                self.CurrentLine = self.CurrentLine + 1
            else:
                raise IndexError("Invalid Memory Access")
        # Pops two, multiples, pushes result. Returns None.
        if tokens[0] == "mul":
            if len(self.stack) >= 2:
                tmp1value = self.stack.pop()
                tmp2value = self.stack.pop()
                self.stack.append(int(tmp1value)*int(tmp2value))
                self.CurrentLine = self.CurrentLine + 1
            else:
                raise IndexError("Invalid Memory Access")
        # Pops two, divides the first by the second, pushes result.
        if tokens[0] == "div":
            if len(self.stack) >= 2:
                tmp1value = self.stack.pop()
                tmp2value = self.stack.pop()
                self.stack.append(int(tmp1value)/int(tmp2value))
                self.CurrentLine = self.CurrentLine + 1
            else:
                raise IndexError("Invalid Memory Access")
        # Pops two, remainder of first divided by second, pushes result.
        if tokens[0] == "mod":
            if len(self.stack) >= 2:
                tmp1value = self.stack.pop()
                tmp2value = self.stack.pop()
                self.stack.append(int(tmp1value) % int(tmp2value))
                self.CurrentLine = self.CurrentLine + 1
            else:
                raise IndexError("Invalid Memory Access")
        # Pops two, if the first value is ZERO, changes the CurrentLine
        # property by the second value. If the first value is not zero, nothing
        # extra occurs. Returns None.
        if tokens[0] == "skip":
            if len(self.stack) >= 2:
                tmp1value = self.stack.pop()
                tmp2value = self.stack.pop()
                if (not tmp1value):
                    self.CurrentLine = self.CurrentLine + tmp2value
                self.CurrentLine = self.CurrentLine + 1
            else:
                raise IndexError("Invalid Memory Access")
        # Pops one, saves that value for future retrival.
        if tokens[0] == "save":
            if len(self.stack) > 0:
                self.saveMemo[int(tokens[1])] = self.stack.pop()
                self.CurrentLine = self.CurrentLine + 1
            else:
                raise IndexError("Invalid Memory Access")
      # Pops zero. Gets a previously saved value and pushes it on the stack
        if tokens[0] == "get":
            if not int (tokens[1]) in self.saveMemo:
                raise IndexError("Invalid Memory Access")
            self.stack.append(self.saveMemo[int(tokens[1])])
            self.CurrentLine = self.CurrentLine + 1
