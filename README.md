Alan Ta
Alan.t.ta.5897@gmail.com

Title:CS320Assignment4
The purpose of this project(s) is to familiarize myself with the Python programming language. This assignment covers python and the functions of tokenization and parsing. 

Requirements:
prog4_1.py
prog4_2.py
prog4_3.py


Configuration/Functions:
    program 4_1 does not require any configurations. This programs function is implemented in Python. The function of this program is to tokenize and then parse an input string. It follows the same rules as program 2_1. It's purpose is to take an input string then tokenize it. It will also provides checks for a valid token. It alse will parse the tokens and output the parsed string. It also provides checks if it is valid. 
    program 4_2 does not require any configurations. This program is implemented in Python. This program will implement a Stack machine. It will accept a list of tokens and will execute the tokens accordingly. When the function finishes, it will property CurrentLine will increment by 1.
    program 4_3 does not require any configurations. This program is implemented in Python.  This program will run with both prog4_2 and prog4_1. It will run the tokenizer and parser then run with an appropriate text file and out print and execute the token functions. 
