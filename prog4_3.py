import sys
import prog4_1
import prog4_2

# Driver that reads command line and executes
def main():
    print("Assignment #4-3, Alan Ta, atta@sdsu.edu")
    filelines = []
    # Opens up the file and reads first command line
    file = open(sys.argv[1], "r")
    for x in file:
    # Brings in and executes x
        filelines.append((x, prog4_1.parse(prog4_1.tokenizer(x))))
    stack = prog4_2.StackMachine()
    try:
        # While stack machine hasnt read all files
        while stack.CurrentLine < len(filelines):
            line = filelines[stack.CurrentLine][1]
            exe = stack.Execute(line)
            if exe is not None:
                print(exe)
    except IndexError as ie:
        print("Line " + str(stack.CurrentLine) + ": '" + " ".join(line) + "' caused " + ie + ".")
    if stack.CurrentLine >= len(filelines):
        print("Program terminated correctly")
    else:
        print("Trying to execute invalid line : " + str(stack.CurrentLine))
if __name__ == "__main__":
 main()
