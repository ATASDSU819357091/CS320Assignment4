# Parses function
def parse(tokens):
    for token in range(0,len(tokens)):
        token = tokens[0]
        if not (token in valid) or len(tokens) != valid[token]:
            raise ValueError("Parse error on line " + token)
    return tokens

# Tokenizer function
def tokenizer(str):
    tokens = str.split()
    for token in range(0,len(tokens)):
        token = tokens[0]
        if not (token in valid) and not validTokens(token):
            raise ValueError("Unexpected token: " + token)
    return tokens
# Checks for invalid tokens
def validTokens(str):
    if len(str) == 0:
        return False
    if (str[0] < '0' or str[0] > '9') and (str[0] != '+' or str[0] != '-'):
        return False
    for i in str[1]:
        if i < '0' or i > '9':
            return False
    return True;

valid = {"push":2, "save":2, "get":2, "pop":1, "add":1, "sub":1, "mul":1, "div":1, "mod":1, "skip":1}
